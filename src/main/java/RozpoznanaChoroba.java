public enum RozpoznanaChoroba {
    GRYPA(1),
    PRZEZIEBIENIE(2),
    BIEGUNKA(3),
    COS_POWAZNEGO(4);

    private int waga;

    RozpoznanaChoroba(int waga){
        this.waga=waga;
    }

    public int getWaga() {
        return waga;
    }
}
