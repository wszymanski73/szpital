public class Patient {
    private String imie;
    private String nazwisko;
    private int jakBardzoZly;
    private RozpoznanaChoroba choroba;
    Patient(){
        this.imie=imie;
        this.nazwisko=nazwisko;
        this.jakBardzoZly=jakBardzoZly;
        this.choroba=getChoroba();
    }
    final String ORDYNATOR="Kowalski";
    boolean ordynator;

    public Patient(boolean czyOrdynator){
        if (czyOrdynator) {
            ordynator = true;
        } else ordynator = false;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getJakBardzoZly() {
        return jakBardzoZly;
    }

    public RozpoznanaChoroba getChoroba() {
        return choroba;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setJakBardzoZly(int jakBardzoZly) {
        this.jakBardzoZly = jakBardzoZly;
    }

    public void setChoroba(RozpoznanaChoroba choroba) {
        this.choroba = choroba;
    }
}
