import java.util.Comparator;

public class PatientZlyKomparator implements Comparator<Patient> {
    @Override
    public int compare(Patient o1, Patient o2) {
        if (o1.getJakBardzoZly() < o2.getJakBardzoZly()) {
            return -1;
        } else if(o1.getJakBardzoZly() > o1.getJakBardzoZly()){
            return 1;
        }else{
            return 0;
        }
    }
}
