import java.util.Comparator;

public class EnumKomparator implements Comparator<RozpoznanaChoroba> {

    @Override
    public int compare(RozpoznanaChoroba o1, RozpoznanaChoroba o2) {
        if (o1.getWaga() < o2.getWaga()) {
            return -1;
        } else if(o1.getWaga() > o1.getWaga()){
            return 1;
        }else{
            return 0;
        }
    }
}
